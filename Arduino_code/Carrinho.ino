
/* Modelo de Código para Controle de Carro c/ Arduino */
 
// Definir os pinos de utilização do Driver L298.
const int IN1 = 1;
const int IN2 = 2;
const int IN3 = 3;
const int IN4 = 4;

 
// Variáveis Úteis
int state_temp;
int vSpeed = 200;   // Define velocidade padrão 0 - 255.
int mSpeed = 150;
char state;

class Motor {
  int in1,in2;

  public:
  
    void SetupPinOutput(int pin1,int pin2){
      in1 = pin1;
      in2 = pin2;
      pinMode(in1,OUTPUT);
      pinMode(in2,OUTPUT);
    }
  
    void Stop(){
      digitalWrite(in1,LOW);
      digitalWrite(in1,LOW);
    }
  
    void Forward(int spd){
      analogWrite(in1,spd);
      digitalWrite(in2,LOW);
    }
  
    void Backward(int spd){
      digitalWrite(in1,LOW);
      analogWrite(in2,spd);
    }
};

Motor MotorL,MotorR;

void setup() {
  // Inicializar as portas como entrada e saída.
  MotorL.SetupPinOutput(IN1,IN2);
  MotorR.SetupPinOutput(IN3,IN4);
 
  // Inicializar a comunicação serial.
  Serial.begin(9600);
  
}
 
void loop() {
 
  // Salva os valores da variável 'state'
  if (Serial.available() > 0) {
    state_temp = Serial.read();
    state = state_temp;
  }
 
  // Se o estado recebido for igual a '8', o carro se movimenta para frente.
  if (state == '8') {
    Serial.println("Comando para Frente");
    MotorL.Forward(vSpeed);
    MotorR.Forward(vSpeed);
  }
    // Se o estado recebido for igual a '7', o carro se movimenta para Frente Esquerda.
  else if (state == '7') {  
    Serial.println("Comando para Frente-Esquerda");
    MotorL.Forward(mSpeed);
    MotorR.Forward(vSpeed);
  }
    // Se o estado recebido for igual a '9', o carro se movimenta para Frente Direita.
  else if (state == '9') {   
    Serial.println("Comando para Frente-Direita");
    MotorL.Forward(vSpeed);
    MotorR.Forward(mSpeed);

  }
  // Se o estado recebido for igual a '2', o carro se movimenta para trás.
  else if (state == '2') { 
    Serial.println("Comando para Trás");
    MotorL.Backward(vSpeed);
    MotorR.Backward(vSpeed);
  }
   // Se o estado recebido for igual a '1', o carro se movimenta para Trás Esquerda.
  else if (state == '1') {  
    Serial.println("Comando para Trás-Esquerda");
    MotorL.Backward(mSpeed);
    MotorR.Backward(vSpeed);
  }
  // Se o estado recebido for igual a '3', o carro se movimenta para Trás Direita.
  else if (state == '3') {  
    Serial.println("Comando para Trás-Direita");
    MotorL.Backward(vSpeed);
    MotorR.Backward(mSpeed);
  }
  // Se o estado recebido for igual a '4', o carro se movimenta para esquerda.
  else if (state == '4') {   
    Serial.print("Comando para Esquerda");
    MotorL.Stop();
    MotorR.Forward(vSpeed);
  }
  // Se o estado recebido for igual a '6', o carro se movimenta para direita.
  else if (state == '6') {   
    Serial.println("Comando para Direita");
    MotorL.Forward(vSpeed);
    MotorR.Stop();
  }
  // Se o estado recebido for igual a '5', o carro permanece parado.
  else if (state == '5') {   
    Serial.print("Comando para Parar");
    MotorL.Stop();
    MotorR.Stop();
  }
}
