import rospy
import tf2_ros
import tf2_msgs.msg
import geometry_msgs.msg
import math

class planeta():
    def __init__(self,label,r,ano):
        self.label = label
        self.tf = geometry_msgs.msg.TransformStamped()
        self.tf.header.frame_id = 'Sol'
        self.tf.child_frame_id = label
        self.r = r
        self.ano = ano
        self.tf.transform.translation.x = r/(2**0.5)
        self.tf.transform.translation.y = r/(2**0.5)
        self.tf.transform.translation.z = 0

        self.tf.transform.rotation.x = 0
        self.tf.transform.rotation.y = 0
        self.tf.transform.rotation.z = 0
        self.tf.transform.rotation.w = 1
    
    def update(self):
        x = 36.5*rospy.Time.now().to_sec()*math.pi*(self.ano**(-1))
        self.tf.transform.translation.x = self.r*math.sin(x)
        self.tf.transform.translation.y = self.r*math.cos(x)
        return self.tf


rospy.init_node('tf_broadcaster',anonymous=True)

br = tf2_ros.TransformBroadcaster()
rate = rospy.Rate(10)

planetas = [planeta('Mercurio',0.5791,88),planeta('Venus',1.082,225),planeta('Terra',1.496,365),planeta('Marte',2.279,687),\
    planeta('Jupiter',7.785,4332),planeta('Saturno',14.34,10760),planeta('Urano',28.71,30681),planeta('Netuno',44.95,165*365)]

while not rospy.is_shutdown():
    for i in planetas:
        br.sendTransform(i.update())
    rate.sleep()

rospy.spin()