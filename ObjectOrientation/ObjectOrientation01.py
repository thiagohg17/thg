class team():
    def __init__(self,name,points,victory,losses,draws,country,coach):
        self.name = name
        self.points = points
        self.victory = victory
        self.losses = losses
        self.draws = draws
        self.country = country
        self.coach = coach

    def __add__(self,other):
        return self.points + other.points

    def __sub__(self,other):
        return self.points - other.points

    def __eq__(self,other):
        return self.points == other.points

    def __le__(self,other):
        return self.points <= other.points

    def __ge__(self,other):
        return self.points >= other.points

    def __gt__(self,other):
        return self.points > other.points


    def __lt__(self,other):
        return self.points < other.points

    def __repr__(self):
        return "O time {} de {} e com seu técnico {} tem {} pontos, {} vitórias, {} empates, {} derrotas.".format(self.name,self.country,self.coach,self.points,self.victory,self.draws,self.losses)

if __name__ == '__main__':
    ## Testing the class Team ##

    a = team('TeamAnime',9,3,0,1,'Japão','Hinata')
    b = team('TeamFarofa',15,4,3,3,'Brasil','Churrasco')
    c = team('TeamRPG',7,2,1,0,'Porão','Mestre dos Magos')
    d = team('TeamVideoGame',13,3,4,3,'Rainbowroad','Mario')

    print(a)
    print(b)
    print(c)
    print(d)

    s1 = a + b
    s2 = a + c
    s3 = d + c

    d1 = b - a
    d2 = c - a
    d3 = d - c

    print(s1,s2,s3)
    print(d1,d2,d3)

    c1 = a > b
    c2 = b > c
    c3 = d > a
    print(c1,c2,c3)

    rank = [a,b,c,d]
    rank.sort()
    ##   end   ##