#!/usr/bin/python
import rospy
import tf2_ros
import tf2_msgs.msg
import geometry_msgs.msg
import math

class planeta():
    def __init__(self,name,radius,primaryBody='Estrela'):
        self.radius = radius

        self.tf = geometry_msgs.msg.TransformStamped()

        self.tf.header.frame_id = primaryBody
        self.tf.child_frame_id = name

        self.tf.transform.translation.x = 0
        self.tf.transform.translation.y = 0
        self.tf.transform.translation.z = 0

        self.tf.transform.rotation.x = 0
        self.tf.transform.rotation.y = 0
        self.tf.transform.rotation.z = 0
        self.tf.transform.rotation.w = 1
    
    def update(self):    
        x = rospy.Time.now().to_sec()*math.pi*(1/(48*self.radius**2))
        self.tf.transform.translation.x = self.radius*math.sin(x)
        self.tf.transform.translation.y = self.radius*math.cos(x)
        return self.tf


rospy.init_node('tf_broadcaster',anonymous=True)
br = tf2_ros.TransformBroadcaster()
rate = rospy.Rate(10)

"""
radius-vulcano, 0.5791
radius-prope 1.082
radius-atrox 1.496
radius-red-moon 0.231
radius-caeli 2.279
radius-caeli-moon 0.109
radius-caeli-meteor 0.252
radius-terra-2 3.354
radius-lua-2 0.143
radius-tantum 7.785
radius-luna 0.4364
"""

planetas = [
    planeta('Vulcano',rospy.get_param("radius-vulcano")),
    planeta('Prope',rospy.get_param("radius-prope")),
    planeta('Atrox',rospy.get_param("radius-atrox")),
    planeta('Red Moon',rospy.get_param("radius-red-moon"),'Atrox'),
    planeta('Caeli',rospy.get_param("radius-caeli")),
    planeta('Caeli Moon',rospy.get_param("radius-caeli-moon"),'Caeli'),
    planeta('Caeli Meteor',rospy.get_param("radius-caeli-meteor"),'Caeli'),
    planeta('Terra 2',rospy.get_param("radius-terra-2")),
    planeta('Lua 2',rospy.get_param("radius-lua-2"),'Terra 2'),
    planeta('Tantum',rospy.get_param("radius-tantum")),
    planeta('Luna',rospy.get_param("radius-luna"),'Tantum'),
]

while not rospy.is_shutdown():
    for i in planetas:
        br.sendTransform(i.update())
    rate.sleep()