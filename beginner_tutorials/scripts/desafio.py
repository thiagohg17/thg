import rospy
from std_msgs.msg import String

def callback(data):
    pass

def listen_and_talk():
    n = 0
    rate = rospy.Rate(10) #10Hz
    pub = rospy.Publisher('chatter', String, queue_size=10)

    while not rospy.is_shutdown():
        n = n%10

        rate = rospy.Rate(10) #10Hz
        rospy.init_node('listener', anonymous=True)
        rospy.init_node('talker', anonymous=True)

        n += 1

if __name__ = '__main__':
    try:
        listen_and_talk()
    except rospy.ROSInterruptException:
        pass
