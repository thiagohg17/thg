# Derivelables Repository #

### Derivelable: Euler Problem ###
Euler/question1.py
Euler/question2.py

### Derivelable: Object Orientation ###
ObjectOrientation/ObjectOrientation01.py

### Derivelable: ROS ###
package begginer_tutorials

### Derivelable: Gazebo ###
Claw_Final/model.sdf
Claw_Final/model.config

### Derivelable: SolarSystem ###
package solar_sys

### Derivelable: Visão ###
Visao/nemo.py
Visao/nemo.mp4

### Derivelable: Arduino ###
Arduino_code/Carrinho.ino

### Derivelable: Sensores ###
Sensores/LeakSensor.pdf
