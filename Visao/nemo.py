import cv2
import time

# Take the video nemo.mp4 in the same folder
vid = cv2.VideoCapture('./nemo.mp4')

# Initiating variables
FPS = 30
currentTime = time.time()*1000

# Tuple with values for HSV colors
light_orange = (1,190,200) 
dark_orange = (18,255,255)
light_white = (0,0,200)
dark_white = (145,60,255)

while True:
    # Update Frame in the appropriate time
    if time.time()*1000 - currentTime > 1000/FPS:
        try:
            currentTime += 1000/FPS

            #read frame/img from the video
            frame = vid.read()[1]

            #change img from BGR to HSV
            frame = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)

            # create 2 masks in the range from light to dark orange and from light to dark white
            mask1 = cv2.inRange(frame, light_orange, dark_orange)
            mask2 = cv2.inRange(frame, light_white, dark_white)
            mask = mask1 + mask2

            # Use the masks to return the frame with just Nemo
            frame = cv2.bitwise_and(frame,frame,mask=mask)

            # return the img to BGR from HSV
            frame = cv2.cvtColor(frame,cv2.COLOR_HSV2BGR)

            # Display the Frame
            cv2.imshow("Frame",frame)

            # Press Q to leave
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        except:
            break

cv2.destroyAllWindows()